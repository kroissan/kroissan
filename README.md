# merkure

Simple tool to create spreadsheet using yaml

## Quickstart

    git clone git@git.easymov.fr:toolkit/merkure.git
    cd merkure
    sudo python3 setup.py develop


## Example

    git clone git@git.easymov.fr:documents/bp.git
    cd bp
    merkure
